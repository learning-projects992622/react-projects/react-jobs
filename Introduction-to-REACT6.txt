Continue  with the delete
1. The same thing in the main file App.jsx, removing all the comments and putting them with code in this .txt file
2. Careful when you pass the props inside the Route element inisde the name of the page
3. install package react toastify, npm i react-toastify, because we want to see when is deleted
4. we will put inside the mainlayout file and were we wanted, we puted inside add also
    Toast is an window that apears up with the message that we want




Before the DELETE method
For a cleaner code I removed every comment from the main file App.jsx

// Introduction to REACT in the Introduction-to-REACT.txt file
// CTRL + SHIFT + L to select all

// import components inside the Introduction-to-REACT2.txt
// we create the new pages inside the new folder pages
import HomePage from "./pages/HomePage";

// main layout
import MainLayout from "./layouts/MainLayout";

// jobs page
import JobsPage from "./pages/JobsPage";

//single  job page with the path="/jobs/:id"
// 8. for the dataLoader import the new async function
// 9. we go in the Route
import JobPage, { jobLoader } from "./pages/JobPage";

// custom 404 not found page
import NotFoundPage from "./pages/NotFoundPage";

// We want more pages on our website and the only way is to add another package for us, using Router: npm i react-router-dom
// importing the Route
import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import AddJobPage from "./pages/AddJobPage";

// This will not do anything whitout the provider
const App = () => {
  // here is the function from element={<AddJobPage addJobSubmit={addJob} />}
  const addJob = async (newJob) => {
    // here we will do our request
    // fetch from were "/api/jobs" and because is a post request we pass a 2nd object
    const response = await fetch("/api/jobs", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newJob),
    });
    return;
  };
  // if we want that this import Route to work we will create a const router
  // (<Route index element={<h1>MyApp</h1>} />)
  // INDEX is the home page
  // we can replace it with path='/chickenWings'

  // replace element={<h1>MyApp</h1>} with the new component HomePage
  // now we call the component inside the HomePage component
  const router = createBrowserRouter(
    createRoutesFromElements(
      // Because we want to use the mainlayout we need to wrap other route inside the main layout
      <Route path="/" element={<MainLayout />}>
        <Route index element={<HomePage />} />
        <Route path="/jobs" element={<JobsPage />} />
        {/* Single JobPage route
      in the path we write the :id */}
        {/* 10. for the dataLoader inside the Route we add the loader  
      11. go to the JobPage.jsx and import the useLoaderData*/}
        <Route path="/jobs/:id" element={<JobPage />} loader={jobLoader} />
        {/* Here we pass the props function which is set to another function addJob, addJobSubmit={addJob} which is created outside the createBrowserRouter()*/}
        <Route path="/add-job" element={<AddJobPage addJobSubmit={addJob} />} />
        {/* path="*" will be like catch all */}
        <Route path="*" element={<NotFoundPage />} />
        <Route />
      </Route>
    )
  );
  //!!!!IMPORTANT because we will use the RouterProvider, i remove the components with the comments
  // all the components are inside the Introduction-to-REACT2.txt

  // without this router={router}, we will not display the {<h1>MyApp</h1>} from the const router
  return <RouterProvider router={router} />;
};

export default App;
