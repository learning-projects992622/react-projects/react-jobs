import React from "react";
import Hero from "../components/Hero";
import HomeCards from "../components/HomeCards";
import JobLisitings from "../components/JobLisitings";
import ViewAllJobs from "../components/ViewAllJobs";

const HomePage = () => {
  // is the same thing like App.jsx
  // in the return we return the components!!
  //   because we want some components/ parts of the website on each page we create an layouts folder with MainLayout.jsx file component
  return (
    <>
      <Hero />
      <HomeCards />
      {/* passing the isHome props to true because we want to see only 3 jobs */}
      <JobLisitings isHome={true} />
      <ViewAllJobs />
    </>
  );
};

export default HomePage;
