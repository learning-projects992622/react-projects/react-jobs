import React from "react";
// use a data Loader from react
// create a fuction in this file
// 1. import { useParams }
// 11. import useLoaderData
// 12. for the DELETE request import useNavigate
import { useParams, useLoaderData, useNavigate } from "react-router-dom";
//import icons
import { FaArrowLeft, FaMapMarker } from "react-icons/fa";
//import link to replace <a> tag
import { Link } from "react-router-dom";
// we need to import toast
import { toast } from "react-toastify";

// 4. here is the props from the deleteJob()
// 5. go to the button delete
const JobPage = ({ deleteJob }) => {
  // 1. first step using the useParams to acces the id and import
  const { id } = useParams();
  // 12. step using the component useLoaderData()
  const job = useLoaderData();

  // 13. DELETE request we need to initialize the useNavigate
  const navigate = useNavigate();

  // 8. create the onDeleteClick(job.id) funct
  const onDeleteClick = (jobId) => {
    const confirm = window.confirm("Are you sure you want to delete this job?");
    // 9. if we don't want to delete, return
    if (!confirm) return;

    // 10.  if we confirm, call the delete functions props with the jobId props which is job.id from the button onClick
    deleteJob(jobId);
    // 11. we want to redirect useNavigate import
    // 12. go up and import

    //toast
    toast.success("Job deleted successfully");

    // 14. after the import and initilaze, redirect
    navigate("/jobs");
  };
  return (
    <>
      {/*Go Back*/}
      <section>
        <div className="container m-auto py-6 px-6">
          <Link
            to="/jobs"
            className="text-indigo-500 hover:text-indigo-600 flex items-center"
          >
            <FaArrowLeft className="mr-2" /> Back to Job Listings
          </Link>
        </div>
      </section>

      <section className="bg-indigo-50">
        <div className="container m-auto py-10 px-6">
          <div className="grid grid-cols-1 md:grid-cols-70/30 w-full gap-6">
            <main>
              <div className="bg-white p-6 rounded-lg shadow-md text-center md:text-left">
                <div className="text-gray-500 mb-4">{job.type}</div>
                <h1 className="text-3xl font-bold mb-4">{job.title}</h1>
                <div className="text-gray-500 mb-4 flex align-middle justify-center md:justify-start">
                  <FaMapMarker className="text-orange-700 text-lg mb-1 mr-1" />
                  <p className="text-orange-700">{job.location}</p>
                </div>
              </div>

              <div className="bg-white p-6 rounded-lg shadow-md mt-6">
                <h3 className="text-indigo-800 text-lg font-bold mb-6">
                  Job Description
                </h3>

                <p className="mb-4">{job.description}</p>

                <h3 className="text-indigo-800 text-lg font-bold mb-2">
                  Salary
                </h3>

                <p className="mb-4">{job.salary} / Year</p>
              </div>
            </main>

            {/* <!-- Sidebar --> */}
            <aside>
              {/* <!-- Company Info --> */}
              <div className="bg-white p-6 rounded-lg shadow-md">
                <h3 className="text-xl font-bold mb-6">Company Info</h3>

                <h2 className="text-2xl">{job.company.name}</h2>

                <p className="my-2">{job.company.description}</p>

                <hr className="my-4" />

                <h3 className="text-xl">Contact Email:</h3>

                <p className="my-2 bg-indigo-100 p-2 font-bold">
                  {job.company.contactEmail}
                </p>

                <h3 className="text-xl">Contact Phone:</h3>

                <p className="my-2 bg-indigo-100 p-2 font-bold">
                  {job.company.contactPhone}
                </p>
              </div>

              {/* <!-- Manage --> */}
              <div className="bg-white p-6 rounded-lg shadow-md mt-6">
                <h3 className="text-xl font-bold mb-6">Manage Job</h3>
                <Link
                  to={`/edit-job/${job.id}`}
                  className="bg-indigo-500 hover:bg-indigo-600 text-white text-center font-bold py-2 px-4 rounded-full w-full focus:outline-none focus:shadow-outline mt-4 block"
                >
                  Edit Job
                </Link>
                {/* 6. Create the event onClick which takes a function and has a function with the parameter job.id 
                7. create the onDeleteClick(job.id) function, go up*/}
                <button
                  onClick={() => onDeleteClick(job.id)}
                  className="bg-red-500 hover:bg-red-600 text-white font-bold py-2 px-4 rounded-full w-full focus:outline-none focus:shadow-outline mt-4 block"
                >
                  Delete Job
                </button>
              </div>
            </aside>
          </div>
        </div>
      </section>
    </>
  );
};
// 2. second step create a async function with an parameter , this is a future of react router not react it self
const jobLoader = async ({ params }) => {
  // 3. here we will make our request
  const res = await fetch(`/api/jobs/${params.id}`);
  // 4. stock the response res
  const data = await res.json();
  // 5. return the data
  return data;
};
// 6. we want to export the function created but also the page by default
// 6. replace export default JobPage; with this
export { JobPage as default, jobLoader };
// 7. we go in the App.jsx file were we have the route first on the import
