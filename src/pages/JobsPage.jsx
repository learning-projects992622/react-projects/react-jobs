import React from "react";
import JobLisitings from "../components/JobLisitings";

// Now after you have the mainlayout, homepage
// for each page that you create NEEEDDDD an ROUTE
const JobsPage = () => {
  // the problem with <JobLisitings /> is that it show only 3 jobs because of the slice()
  // we must change the function
  return (
    <section className="bg-blue-50 px-4 py-6">
      <JobLisitings />
    </section>
  );
};

export default JobsPage;
