import React from "react";
// This card component will take as a props the {children}
// children is a key word for a component used to wrap
// Used inside the HomeCards.jsx file will have this div for each card that we use the component
const Card = ({ children, bg = "bg-gray-100" }) => {
  return (
    // using a template literal we can pass inside the className the paramatter bg
    // Now we have bg by default but we can change it inside the HomeCards.jsx <Card bg = "bg-indingo-100>"
    <div className={`${bg} p-6 rounded-lg shadow-md`}>{children}</div>
  );
};

export default Card;
