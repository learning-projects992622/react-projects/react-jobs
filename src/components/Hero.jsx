import React from "react";
// 1. const Hero = (props) => { using the props, declare inside the App.jsx inside the Hero tag <Hero title="Title" subtitle="This is the subtitle" />

// 2. For a better structure of the props we can create the parameter inside the function { title, subtitle }, MANDATORY the parametter between the {param1, param2}

// 3. Default props
//   {title = "Default parametter REACT",
//   subtitle = "Find the job that fits your skills", }
// If we are using this we don't have to define inside the App.jsx file inside the Hero tag <Hero title="Title" subtitle="This is the subtitle" />

const Hero = ({
  title = "REACT",
  subtitle = "Find the job that fits your skills",
}) => {
  return (
    <div>
      <section className="bg-indigo-700 py-20 mb-4">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 flex flex-col items-center">
          <div className="text-center">
            {/* 1. after declaring the props and pasing the props as an parametter inside the arrow functon we can use them here {props.title}*/}
            {/* 2. better structure {title} */}
            <h1 className="text-4xl font-extrabold text-white sm:text-5xl md:text-6xl">
              {title}
            </h1>
            <p className="my-4 text-xl text-white">{subtitle}</p>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Hero;
