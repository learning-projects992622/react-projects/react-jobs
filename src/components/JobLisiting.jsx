import React from "react";
// We want to add useState because we don't want too see full description on the jobs
import { useState } from "react";
import { FaMapMarker } from "react-icons/fa";
import { Link } from "react-router-dom";

// props betweeen the { job }
const JobLisiting = ({ job }) => {
  // This will be for the show full description with a default value "false"
  // between the [] we have the NAME of the state and the second will be the NAME of the FUNCTION that we will call to change that state
  const [showFullDescription, setShowFullDescription] = useState(false);

  // 1st step in using the useState
  // variable to stock what we want to change, in our case job.description
  let description = job.description;

  //now an if statement for our useState name
  // because by default useState is false we are using this condition (!showFullDescription)
  if (!showFullDescription) {
    // this "new" description variable will be the description, no more than 90 characters substring(0,90) and appent the "..."
    description = description.substring(0, 90) + "...";
  }
  // now we can change the {job.description} with the variable description

  // 2nd stept of useState
  // because we want a button to see full description if needed
  // we do this in the return
  return (
    <div className="bg-white rounded-xl shadow-md relative">
      <div className="p-4">
        <div className="mb-6">
          <div className="text-gray-600 my-2">{job.type}</div>
          <h3 className="text-xl font-bold">{job.title}</h3>
        </div>

        <div className="mb-5">{description}</div>
        {/* - here is the second step of the description 
         - between button tags we add an ternary condition {showFullDescription ? "Less" : "More"} this will show Less because of our condition from the 1st step
         - now we need to add an event but we cannot call our function direct setShowFullDescription, we must create another function noname and return our function we have as parametter another function noname with another parametter prevState which will return the negation when click on the button*/}
        <button
          onClick={() => setShowFullDescription((prevState) => !prevState)}
          className="text-indigo-500 mb-5 hover:text-indigo-600"
        >
          {showFullDescription ? "Less" : "More"}
        </button>
        {/* When the component first renders, showFullDescription is false by default.
The description is truncated to the first 90 characters, and "..." is appended.
When the button is clicked, setShowFullDescription toggles the state.
The component re-renders with the updated state:
If showFullDescription is true, the full description is displayed, and the button text shows "Less".
If showFullDescription is false, the truncated description is displayed, and the button text shows "More". */}
        <h3 className="text-indigo-500 mb-2">{job.salary} / Year</h3>

        <div className="border border-gray-100 mb-5"></div>

        <div className="flex flex-col lg:flex-row justify-between mb-4">
          <div className="text-orange-700 mb-3">
            {/* for dislaying an icon we can 
            1. install npm i react-icons 
            2. import the icon: import { FaMapMarker } from "react-icons/fa";
            3. change <i> tag with the component imported <FaMapMarker/>*/}
            <FaMapMarker className="inline text-lg mb-1 mr-1" />
            {job.location}
          </div>
          {/* again using the template literal because we want, when we click on the job, to open only that job */}
          <Link
            to={`/jobs/${job.id}`}
            className="h-[36px] bg-indigo-500 hover:bg-indigo-600 text-white px-4 py-2 rounded-lg text-center text-sm"
          >
            Read More
          </Link>
        </div>
      </div>
    </div>
  );
};

export default JobLisiting;
