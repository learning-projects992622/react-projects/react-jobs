import logo from "../assets/images/logo.png";
// NavLink works in the same way but adds an class to the active link
import { NavLink } from "react-router-dom";
// We want a better way to change the pages
// For this we import Link component
// Change the <a><a/> tag with <Link></Link> and the 'href' with 'to'
const Navbar = () => {
  const linkClass = ({ isActive }) =>
    isActive
      ? "bg-black text-white hover:bg-gray-900 hover:text-white rounded-md px-3 py-2"
      : "text-white hover:bg-gray-900 hover:text-white rounded-md px-3 py-2";
  return (
    <nav className="bg-indigo-700 border-b border-indigo-500">
      <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
        <div className="flex h-20 items-center justify-between">
          <div className="flex flex-1 items-center justify-center md:items-stretch md:justify-start">
            {/* <!-- Logo --> */}
            <NavLink className="flex flex-shrink-0 items-center mr-4" to="/">
              {/* making dynamic src={logo} */}
              <img className="h-10 w-auto" src={logo} alt="React Jobs" />
              <span className="hidden md:block text-white text-2xl font-bold ml-2">
                React Jobs
              </span>
            </NavLink>
            <div className="md:ml-auto">
              <div className="flex space-x-2">
                {/* Here we make our first change active link
                1. remove the className value "text-white bg-black hover:bg-gray-900 hover:text-white rounded-md px-3 py-2" 
                2. replacing with linkClass which we created before the return !!! for a much cleaner code*/}
                <NavLink to="/" className={linkClass}>
                  Home
                </NavLink>
                {/* The href must be the same with the route path */}
                <NavLink to="/jobs" className={linkClass}>
                  Jobs
                </NavLink>
                <NavLink to="/add-job" className={linkClass}>
                  Add Job
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
