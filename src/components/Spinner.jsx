import React from "react";
import { ClipLoader } from "react-spinners";

const override = {
  display: "block",
  margin: "100px auto",
};
const Spinner = ({ loading }) => {
  return (
    // ClipLoader is an component already made
    // inside we add the props
    <ClipLoader
      color="#4338ca"
      loading={loading}
      cssOverride={override}
      size={150}
    />
  );
  //   after finish we will add this component with the props were we need
  // in our case we will add inside the JobListings.jsx
};

export default Spinner;
