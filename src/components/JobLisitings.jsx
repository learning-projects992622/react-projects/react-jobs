import React from "react";
// everything inside Introduction-to-REACT3.txt before using the json-server, comments and code
import JobLisiting from "./JobLisiting";
// using json-server we need useState and useEffect
import { useState, useEffect } from "react";
// after creating the Spinner component import
import Spinner from "./Spinner";

const JobLisitings = ({ isHome = false }) => {
  // useState needs name and function name and the default value will be a empty array [] because will be fill with the response from the API
  const [jobs, setJobs] = useState([]);
  // need loading useState to true when is fetching the data
  const [loading, setLoading] = useState(true);
  // useEffect takes a function and an dependecie empty array BECAUSE if NOT it will be and endless loop
  // inside the function we will do our fetching
  useEffect(() => {
    const fetchJobs = async () => {
      // Because we want 3 jobs in the home page and all jobs in the jobs page we will do a ternary operator inside a variable
      // Because we use an PROXY created inside the vite.config.js in server, we replaced the "http://localhost:8000" with the proxy created by us "/api"
      const apiUrl = isHome ? "/api/jobs?_limit=3" : "/api/jobs";
      // and we replace the url with the variable
      try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        setJobs(data);
      } catch (error) {
        console.log("Error fetching data", error);
      } finally {
        setLoading(false);
      }
    };
    fetchJobs();
  }, []);
  return (
    <div>
      <section className="bg-blue-50 px-4 py-10">
        <div className="container-xl lg:container m-auto">
          <h2 className="text-3xl font-bold text-indigo-500 mb-6 text-center">
            {isHome ? "Recent Jobs" : "Browse Jobs"}
          </h2>

          {/* repace the text<h2>Loading...</h2> with the component with the props 
          We can use this spinner were we fetch data*/}
          {loading ? (
            <Spinner loading={loading} />
          ) : (
            <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
              {jobs.map((job) => (
                <JobLisiting key={job.id} job={job} />
              ))}
            </div>
          )}
        </div>
      </section>
    </div>
  );
};

export default JobLisitings;
