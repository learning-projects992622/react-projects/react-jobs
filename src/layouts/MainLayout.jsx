import React from "react";
// we need to import the outlet
// Outlet is a natural component already made by React
// the <Outlet> component offers an easier solution to render layouts with nested UI
import { Outlet } from "react-router-dom";
import Navbar from "../components/Navbar";
// here we import the toast container
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const MainLayout = () => {
  return (
    <>
      <Navbar />
      {/* Components created above the natural component */}
      <Outlet />
      {/* we can put this were we want because is absolute position */}
      <ToastContainer />
    </>
  );
};

export default MainLayout;
